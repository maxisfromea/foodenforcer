//
//  AppDelegate.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    private var appCoordinator: AppCoordinator?
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let appWindow = UIWindow(frame: UIScreen.main.bounds)
        appCoordinator = AppDIContainer().makeAppCoordinator(window: appWindow)
        window = appWindow
        appCoordinator?.start()
        
        return true
    }
}

