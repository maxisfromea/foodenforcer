//
//  Food.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//

import Foundation

struct FoodResponseDTO: Decodable {
    let results: [Food]
}

struct Food: Decodable {
    enum CodingKeys: String, CodingKey {
        case productDescription = "product_description"
    }
    
    let productDescription: String
}
