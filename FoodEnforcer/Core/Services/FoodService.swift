//
//  FoodService.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//

import Foundation

enum FoodServiceError: Error {
    case invalidURL
    case custom(Error)
    case noData
    case parsing
    
    var message: String {
        switch self {
        case .parsing: return "Unexpected response from server"
        case .noData: return "Did not receive data from server"
        case .custom(let error): return error.localizedDescription
        case .invalidURL: return "Invalid request URL"
        }
    }
}

typealias FoodServiceCompletion = (Result<[Food], FoodServiceError>) -> Void

protocol FoodService {
    func getList(limit: String, completion: @escaping FoodServiceCompletion)
}

final class DefaultFoodService: FoodService {
    private let urlString: String
    private let session: URLSession
    
    init(session: URLSession = .shared, urlString: String) {
        self.session = session
        self.urlString = urlString
    }
    
    func getList(limit: String, completion: @escaping FoodServiceCompletion) {
        do {
            let url = try makeFinalURL(limit: limit)
            session
                .dataTask(with: url) { data, response, error in
                    self.handleResponse(data: data, error: error, completion: completion)
                }
                .resume()
        } catch FoodServiceError.invalidURL {
            completion(.failure(.invalidURL))
        } catch {
            completion(.failure(.custom(error)))
        }
    }
}

// MARK: - Private

private extension DefaultFoodService {
    func makeFinalURL(limit: String) throws -> URL {
        let queryItems = [URLQueryItem(name: "limit", value: limit)]
        var urlComponents = URLComponents(string: urlString)
        urlComponents?.queryItems = queryItems
        
        guard let url = urlComponents?.url else { throw FoodServiceError.invalidURL }
        
        return url
    }
    
    func handleResponse(data: Data?, error: Error?, completion: @escaping FoodServiceCompletion) {
        DispatchQueue.main.async {
            if let error = error {
                completion(.failure(FoodServiceError.custom(error)))
            } else {
                self.parse(data, completion: completion)
            }
        }
    }
    
    func parse(_ data: Data?, completion: @escaping FoodServiceCompletion) {
        guard let data = data else {
            completion(.failure(.noData))
            return
        }
        
        do {
            let object = try JSONDecoder().decode(FoodResponseDTO.self, from: data)
            completion(.success(object.results))
        } catch {
            #if DEBUG
            print("=== Parsing Error \(error) ===")
            #endif
            completion(.failure(FoodServiceError.parsing))
        }
    }
}
