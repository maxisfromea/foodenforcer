//
//  PasscodeValidationService.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//

import Foundation

protocol PasscodeValidationService {
    func validate(_ pin: String, completion: @escaping (Bool) -> Void)
}

final class DefaultPasscodeValidationService: PasscodeValidationService {
    private let validPin = "1111"
    
    func validate(_ pin: String, completion: @escaping (Bool) -> Void) {
        completion(pin == validPin)
    }
}
