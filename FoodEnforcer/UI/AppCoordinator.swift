//
//  AppCoordinator.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//

import Foundation
import UIKit

protocol AppCoordinatorDependencies {
    func makeFoodScene() -> UIViewController
    func makePasscodeScene(actions: PasscodeViewModelActions) -> UIViewController
}

final class AppCoordinator: BaseCoordinator {
    private let window: UIWindow
    private let dependencies: AppCoordinatorDependencies
    private let rootController = UINavigationController()
    
    init(window: UIWindow, dependencies: AppCoordinatorDependencies) {
        self.window = window
        self.dependencies = dependencies
        
        window.rootViewController = rootController
        window.makeKeyAndVisible()
    }
    
    override func start() {
        let viewController = dependencies.makePasscodeScene(
            actions: PasscodeViewModelActions(
                onContinue: { [weak self] in
                    self?.showFoodList()
                }
            )
        )
        rootController.show(viewController, sender: self)
    }
}

// MARK: - Private

private extension AppCoordinator {
    func showFoodList() {
        let viewController = dependencies.makeFoodScene()
        rootController.pushViewController(viewController, animated: true)
    }
}
