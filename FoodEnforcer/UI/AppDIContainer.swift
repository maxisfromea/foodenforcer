//
//  AppDIContainer.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//

import Foundation
import UIKit


final class AppDIContainer {
    private let urlString = "https://api.fda.gov/food/enforcement.json"
    
    private lazy var passcodeValidateService = DefaultPasscodeValidationService()
    private lazy var foodService = DefaultFoodService(urlString: urlString)
    
    func makeAppCoordinator(window: UIWindow) -> AppCoordinator {
        AppCoordinator(window: window, dependencies: self)
    }
}

// MARK: - AppCoordinatorDependencies

extension AppDIContainer: AppCoordinatorDependencies {
    func makeFoodScene() -> UIViewController {
        let viewController = FoodListViewController.instantiateViewController()
        viewController.viewModel = DefaultFoodListViewModel(
            foodService: foodService
        )
        return viewController
    }

    func makePasscodeScene(actions: PasscodeViewModelActions) -> UIViewController {
        let viewController = PasscodeViewController.instantiateViewController()
        viewController.viewModel = DefaultPasscodeViewModel(
            passcodeService: passcodeValidateService,
            actions: actions
        )
        return viewController
    }
}
