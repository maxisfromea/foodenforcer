//
//  FoodCell.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//

import UIKit

class FoodCell: UITableViewCell {
    static let reusableIdentifier = "FoodCell"
    
    @IBOutlet private weak var labelTitle: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        text = nil
    }
    
    var text: String? {
        didSet {
            labelTitle.text = text
        }
    }
}
