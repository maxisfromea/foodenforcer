//
//  FoodListViewController.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//  Copyright (c) 2022 All rights reserved.
//

import UIKit

class FoodListViewController: UIViewController {
    private let reuseIdentifier = "FooCell"
    
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: FoodListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupBindings()
        
        viewModel.viewDidLoad()
    }
}

// MARK: - User actions

private extension FoodListViewController {
    @IBAction private func didTapLimit(_ sender: UIButton) {
        guard let title = sender.title(for: .normal) else { return }
        
        viewModel.didTapLimit(title)
    }
    
    @objc private func didPullToRefresh() {
        viewModel.didPullToRefresh()
        tableView.refreshControl?.endRefreshing()
    }
}

// MARK: - UITableViewDataSource

extension FoodListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let title = viewModel.title(at: indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodCell.reusableIdentifier) as? FoodCell
        else {
            return UITableViewCell()
        }
        
        cell.text = title
        return cell
    }
}

// MARK: - Private

private extension FoodListViewController {
    func setupUI() {
        tableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        title = viewModel.screenTitle
    }
    
    func setupBindings() {
        viewModel.onReload = { [weak self] in self?.tableView.reloadData() }
        viewModel.onError = { [weak self] in self?.showError($0) }
        viewModel.onLoadingStateChange = { [weak self] in self?.setLoadingState($0) }
    }
    
    func setLoadingState(_ state: LoadingState) {
        if state == .startLoading {
            loadingIndicator.startAnimating()
            loadingIndicator.isHidden = false
            view.bringSubviewToFront(loadingIndicator)
        } else {
            loadingIndicator.stopAnimating()
            view.sendSubviewToBack(loadingIndicator)
        }
    }
    
    func showError(_ error: String) {
        let alertAcontroller = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alertAcontroller.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertAcontroller, animated: true)
    }
}
