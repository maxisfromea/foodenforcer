//
//  FoodListViewModel.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//  Copyright (c) 2022 All rights reserved.
//

import Foundation

enum LoadingState {
    case startLoading
    case stopLoading
}

protocol FoodListViewModelInput {
    func viewDidLoad()
    func didPullToRefresh()
    func didTapLimit(_ limit: String)
}

protocol FoodListViewModelOutput {
    var onLoadingStateChange: ((LoadingState) -> Void)? { get set }
    var onError: ((String) -> Void)? { get set }
    var onReload: (() -> Void)? { get set }
    
    var numberOfRows: Int { get }
    func title(at indexPath: IndexPath) -> String?
    
    var screenTitle: String { get }
}

protocol FoodListViewModel: FoodListViewModelInput, FoodListViewModelOutput { }

class DefaultFoodListViewModel: FoodListViewModel {
    private var currentLimit = "20"
    
    private let foodService: FoodService
    
    private var dataSource = [String]()
    
    let screenTitle = "Foods"
    
    var onLoadingStateChange: ((LoadingState) -> Void)?
    var onError: ((String) -> Void)?
    var onReload: (() -> Void)?
    
    var numberOfRows: Int { dataSource.count }
    
    init(foodService: FoodService) {
        self.foodService = foodService
    }
    
    // MARK: - OUTPUT

    func title(at indexPath: IndexPath) -> String? {
        guard dataSource.indices.contains(indexPath.row) else { return nil }
        
        return dataSource[indexPath.row]
    }
}

// MARK: - INPUT. View event methods

extension DefaultFoodListViewModel {
    func viewDidLoad() {
        loadData(limit: currentLimit)
    }
    
    func didTapLimit(_ limit: String) {
        guard currentLimit != limit else { return }
        
        currentLimit = limit
        loadData(limit: currentLimit)
    }
    
    func didPullToRefresh() {
        loadData(limit: currentLimit)
    }
}

// MARK: - Private

private extension DefaultFoodListViewModel {
    func loadData(limit: String) {
        onLoadingStateChange?(.startLoading)
        
        foodService.getList(limit: limit) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.onError?(error.message)
            case .success(let food):
                self?.setupDataSource(food: food)
            }
            self?.onLoadingStateChange?(.stopLoading)
        }
    }
    
    func setupDataSource(food: [Food]) {
        dataSource = food.map { $0.productDescription }
        
        onReload?()
    }
}
