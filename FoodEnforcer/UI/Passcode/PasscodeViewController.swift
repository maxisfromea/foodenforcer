//
//  PasscodeViewController.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//  Copyright (c) 2022 All rights reserved.
//

import UIKit

class PasscodeViewController: UIViewController {
    @IBOutlet private weak var textField: UITextField!
    
    private lazy var shakeAnimation: CAKeyframeAnimation = {
        let translation = CAKeyframeAnimation(keyPath: "transform.translation.x");
        translation.timingFunction = CAMediaTimingFunction(name: .linear)
        translation.values = [-5, 5, -5, 5, -3, 3, -2, 2, 0]
        translation.duration = 0.3
        return translation
    }()
    
    var viewModel: PasscodeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupBindings()
    }
}

// MARK: - User actions

private extension PasscodeViewController {
    @IBAction private func didTapDigit(_ sender: UIButton) {
        guard let digit = sender.title(for: .normal) else { return }
        
        viewModel.didEnter(digit)
    }
    
    @IBAction private func didTapDelete(_ sender: UIButton) {
        viewModel.didDeleteDigit()
    }
}

// MARK: - Private

private extension PasscodeViewController {
    func setupBindings() {
        viewModel.onNewText = { [weak self] in self?.textField.text = $0 }
        viewModel.onPinError = { [weak self] in self?.handlePinError() }
    }
    
    func setupUI() {
        title = viewModel.screenTitle
        textField.defaultTextAttributes.updateValue(13, forKey: .kern)
    }
    
    func handlePinError() {
        textField.layer.add(shakeAnimation, forKey: "shake")
    }
}
