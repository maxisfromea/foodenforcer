//
//  PasscodeViewModel.swift
//  FoodEnforcer
//
//  Created by Maximus on 08/04/2022.
//  Copyright (c) 2022 All rights reserved.
//

import Foundation

protocol PasscodeViewModelInput {
    func didEnter(_ digit: String)
    func didDeleteDigit()
}

protocol PasscodeViewModelOutput {
    var screenTitle: String { get }
    
    var onNewText: ((String) -> Void)? { get set }
    var onPinError: (() -> Void)? { get set }
}

struct PasscodeViewModelActions {
    let onContinue: () -> ()
}

protocol PasscodeViewModel: PasscodeViewModelInput, PasscodeViewModelOutput { }

class DefaultPasscodeViewModel: PasscodeViewModel {
    private let passcodeService: PasscodeValidationService
    private let actions: PasscodeViewModelActions
    
    private var pin = ""
    
    let screenTitle = "Passcode"
    
    var onNewText: ((String) -> Void)?
    var onPinError: (() -> Void)?
    
    init(passcodeService: PasscodeValidationService, actions: PasscodeViewModelActions) {
        self.passcodeService = passcodeService
        self.actions = actions
    }
    
    // MARK: - OUTPUT

}

// MARK: - INPUT. View event methods

extension DefaultPasscodeViewModel {
    func didEnter(_ digit: String) {
        if pin.count < 4 {
            pin += digit
        }
        onNewText?(pin)
        
        if pin.count == 4 {
            validate()
        }
    }
    
    func didDeleteDigit() {
        pin.popLast()
        
        onNewText?(pin)
    }
}

// MARK: - Private

extension DefaultPasscodeViewModel {
    func validate() {
        passcodeService.validate(pin) { [weak self] isValid in
            defer {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                    self?.pin = ""
                    self?.onNewText?("")
                }
            }
            
            if isValid {
                self?.actions.onContinue()
            } else {
                self?.onPinError?()
            }
        }
    }
}
