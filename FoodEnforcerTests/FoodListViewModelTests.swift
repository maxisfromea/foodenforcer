//
//  FoodListViewModelTests.swift
//  FoodEnforcerTests
//
//  Created by Maximus on 08/04/2022.
//

import XCTest
@testable import FoodEnforcer

final class FakeFoodService: FoodService {
    private let result: Result<[Food], FoodServiceError>
    
    init(result: Result<[Food], FoodServiceError>) {
        self.result = result
    }
    
    var numberOfCalls = 0
    var limit = ""
    func getList(limit: String, completion: @escaping FoodServiceCompletion) {
        numberOfCalls += 1
        self.limit = limit
        completion(result)
    }
}

enum DummyError: LocalizedError {
    case dummy
    
    var errorDescription: String? { "Dummy" }
}

class FoodListViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_onEmptyDataSource_numberOfRows_returnsZero() {
        XCTAssertEqual(makeSUT().numberOfRows, 0)
    }
    
    func test_onNonEmptyDataSource_numberOfRows_returnsExpectedValue() async {
        let sut = makeSUT(result: .success([Food(productDescription: "Foo")]))
        sut.viewDidLoad()
        XCTAssertEqual(sut.numberOfRows, 1)
    }
    
    func test_onEmptyDataSource_titleAtIndexPath_returnsNil() {
        XCTAssertNil(makeSUT().title(at:IndexPath(row: 0, section: 0)))
    }
    
    func test_onNonEmptyDataSource_titleAtIndexPath_returnsExpectedValue() async {
        let foods = [Food(productDescription: "Foo"), Food(productDescription: "Bar")]
        let sut = makeSUT(result: .success(foods))
        sut.viewDidLoad()
        
        XCTAssertEqual(sut.title(at: IndexPath(row: 0, section: 0)), foods.first?.productDescription)
        XCTAssertEqual(sut.title(at: IndexPath(row: 1, section: 0)), foods.last?.productDescription)
    }
    
    func test_onViewDidLoad_foodServiceIsCalled_withDefaultLimit() async {
        let service = FakeFoodService(result: .success([]))
        let sut = makeSUT(foodService: service)
        sut.viewDidLoad()
        
        XCTAssertEqual(service.limit, "20")
        XCTAssertEqual(service.numberOfCalls, 1)
    }
    
    func test_withDifferentCurrentAndSelectedLimit_onDidTapLimit_foodServiceIsCalled_withCoorectLimit() {
        let service = FakeFoodService(result: .success([]))
        let sut = makeSUT(foodService: service)
        sut.didTapLimit("2")
        
        XCTAssertEqual(service.numberOfCalls, 1)
        XCTAssertEqual(service.limit, "2")
    }
    
    func test_withSameCurrentAndSelectedLimit_onDidTapLimit_foodServiceIsNotCalled() {
        let service = FakeFoodService(result: .success([]))
        let sut = makeSUT(foodService: service)
        sut.didTapLimit("20")
        
        XCTAssertEqual(service.numberOfCalls, 0)
    }
    
    func test_didPullToRefresh_callsFoodService_withCurrentLimit() async {
        let service = FakeFoodService(result: .success([]))
        let sut = makeSUT(foodService: service)
        sut.didTapLimit("10")
        sut.didTapLimit("2")
        sut.didPullToRefresh()
        
        XCTAssertEqual(service.limit, "2")
    }
    
    func test_onViewDidLoad_onLoadingStateChangeIsCalled_withCorrectStates() async {
        let sut = makeSUT()
        var counter = 0
        sut.onLoadingStateChange = {
            if counter == 0 {
                XCTAssertEqual($0, .startLoading)
            } else {
                XCTAssertEqual($0, .stopLoading)
            }
            counter += 1
        }
        sut.viewDidLoad()
    }
    
    func test_onFoodServiceError_onReloadIsNotCalled() async {
        let sut = makeSUT(result: .failure(.noData))
        sut.onReload = { XCTFail("This should not be called") }
        sut.viewDidLoad()
    }
    
    func test_onFoodServiceNoDataError_onErrorHasExpectedMessage() async {
        let sut = makeSUT(result: .failure(.noData))
        sut.onError = { XCTAssertEqual($0, "Did not receive data from server") }
        sut.viewDidLoad()
    }
    
    func test_onFoodServiceParsingError_onErrorHasExpectedMessage() async {
        let sut = makeSUT(result: .failure(.parsing))
        sut.onError = { XCTAssertEqual($0, "Unexpected response from server") }
        sut.viewDidLoad()
    }
    
    func test_onFoodServiceInvalidURLError_onErrorHasExpectedMessage() async {
        let sut = makeSUT(result: .failure(.invalidURL))
        sut.onError = { XCTAssertEqual($0, "Invalid request URL") }
        sut.viewDidLoad()
    }
    
    func test_onFoodServiceCustomError_onErrorHasExpectedMessage() async {
        let sut = makeSUT(result: .failure(.custom(DummyError.dummy)))
        sut.onError = { XCTAssertEqual($0, "Dummy") }
        sut.viewDidLoad()
    }
}

private extension FoodListViewModelTests {
    func makeSUT(result: Result<[Food], FoodServiceError> = .success([])) -> DefaultFoodListViewModel {
        DefaultFoodListViewModel(foodService: FakeFoodService(result: result))
    }
    
    func makeSUT(foodService: FoodService) -> DefaultFoodListViewModel {
        DefaultFoodListViewModel(foodService: foodService)
    }
}
