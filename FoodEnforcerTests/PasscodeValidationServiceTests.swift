//
//  PasscodeValidationServiceTests.swift
//  FoodEnforcerTests
//
//  Created by Maximus on 08/04/2022.
//

import XCTest
@testable import FoodEnforcer

class PasscodeValidationServiceTests: XCTestCase {

    func test_onValidPin_passcodeService_completesWithTrue() async {
        let sut = DefaultPasscodeValidationService()
        sut.validate("1111") { XCTAssertTrue($0) }
    }

    func test_onInvalidPin_passcodeService_completesWithFalse() async {
        let sut = DefaultPasscodeValidationService()
        sut.validate("1234") { XCTAssertFalse($0) }
    }
}
